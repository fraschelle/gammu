GRANT USAGE ON *.* TO 'smsd'@'localhost' IDENTIFIED BY 'password';

GRANT SELECT, INSERT, UPDATE, DELETE ON `smsd`.* TO 'smsd'@'localhost';

CREATE DATABASE smsd;
