# utiliser gammu

Identifier : `gammu identify`

Vérifier le statut : `gammu getsecuritystatus`

Entrer le code PIN : `gammu entersecuritycode PIN -`

Récupérer les messages : `gammu getallsms`

Supprimer les messages : `gammu deleteallsms 1`
essayer les autres entiers ... 1 (Inbox SIM), 2 (Outbox SIM), 3 (Inbox), 4 (Outbox)

Envoyer un message : `gammu sendsms TEXT 123456 -text "All your base are belong to us"`
